/* ----------------------------------------------------------------------------------
 * Eco Plug ESP Pinout:
 * ----------------------------------------------------------------------------------
 *            -------------------
 *          --|             GND |--
 *          --| VCC             |--
 *          --| Reset           |--
 *          --|                 |--
 *          --|              TX |-- Serial TX/Prog RX
 *          --|              RX |-- Serial RX/Prog TX
 *   Button --| GPIO13    GPIO5 |--
 *    Relay --| GPIO15    GPIO4 |--
 * Blue LED --| GPIO2     GPIO0 |-- Bootloader (low - program, high - normal)
 *            -------------------
 * ----------------------------------------------------------------------------------
   Board: Generic ESP8266 Module
   CPU Frequency: 80 Mhz
   Flash Size: 1M (128K SPIFFS)
   Web UI files in SPIFFS
 */

#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <ESP8266WebServer.h>
#include <ESP8266HTTPUpdateServerSPIFFS.h>
#include <WiFiManager.h>
#include <DNSServer.h>
#include <WiFiClient.h>
#include <PubSubClient.h>
#include <EEPROM.h>
#include <FS.h>
#include <Ticker.h>
#include <Button.h>
#include <TimeLib.h>
#include <Timezone.h>

#define LED_PIN             2    // active low
#define RELAY_PIN           15   // active high
#define BUTTON_PIN          13   // active low

#define ON                  true
#define OFF                 false

#define NTP_SYNC_INTERVAL   21600   // 6 hours
#define NTP_POOL            "us.pool.ntp.org"

#define SETTINGS_REV        0xA2
#define MAX_STRING_LENGTH   128

enum EEPROMSettings
{
  SETTING_INITIALIZED,
  SETTING_AUTO_ON,
  SETTING_ON_ENABLE,
  SETTING_ON_HOUR,
  SETTING_ON_MIN,
  SETTING_OFF_ENABLE,
  SETTING_OFF_HOUR,
  SETTING_OFF_MIN,
  SETTING_TIMEZONESTD,
  SETTING_TIMEZONEDST,
  SETTING_MQTT_ENABLE,
  SETTING_DELIM,
  // Max 128 characters for EEPROM stored strings below...
  SETTING_NAME_LENGTH,
  SETTING_NAME,
  SETTING_MDNS_LENGTH = SETTING_NAME + MAX_STRING_LENGTH,
  SETTING_MDNS,
  SETTING_MQTT_SERVER_LENGTH = SETTING_MDNS + MAX_STRING_LENGTH,
  SETTING_MQTT_SERVER,
  SETTING_MQTT_USER_LENGTH = SETTING_MQTT_SERVER + MAX_STRING_LENGTH,
  SETTING_MQTT_USER,
  SETTING_MQTT_PASSWORD_LENGTH = SETTING_MQTT_USER + MAX_STRING_LENGTH,
  SETTING_MQTT_PASSWORD,
  NUM_OF_SETTINGS = SETTING_MQTT_PASSWORD + MAX_STRING_LENGTH
};

bool autoOn = false;
bool onEnable = false;
bool offEnable = false;
bool isTimeSet = false;
bool relayState = false;

uint32_t onTime = 0;
uint32_t offTime = 0;
time_t nextOnTime = 0;
time_t nextOffTime = 0;

String outletName = "Wifi Outlet";
String mdnsName;

MDNSResponder mdns;
ESP8266WebServer server(80);
ESP8266HTTPUpdateServer httpUpdater;

bool mqttEnable = false;
bool mqttUpdate = false;
String mqttServer = "";
String mqttUser = "";
String mqttPassword = "";
WiFiClient mqttClient;
PubSubClient mqtt(mqttClient);

TimeChangeRule timeDST = {"DST", Second, Sun, Mar, 2, 0};
TimeChangeRule timeSTD = {"STD", First, Sun, Nov, 2, 0};
Timezone myTZ(timeDST, timeSTD);
TimeChangeRule *tcr;

Ticker blinkLED_tckr, checkTime_tckr;
Button btn(BUTTON_PIN, true, true, 100);

void setup()
{
  WiFiManager wifiManager;

  // Get MAC address and build AP SSID
  uint8_t macAddr[6];
  char apName[13];
  WiFi.macAddress(macAddr);
  sprintf(apName, "outlet-%02x%02x%02x", macAddr[3], macAddr[4], macAddr[5]);
  mdnsName = apName;

  // Initialize and read EEPROM settings
  EEPROM.begin(NUM_OF_SETTINGS);
  readEEPROMSettings();

  // Initialize SPIFFS
  SPIFFS.begin();

  // Setup and initialize pins
  pinMode(LED_PIN, OUTPUT);
  pinMode(RELAY_PIN, OUTPUT);

  // If button pressed (active low), factory reset
  if (!digitalRead(BUTTON_PIN))
  {
    blinkLED_tckr.attach_ms(250, blinkLED);
    wifiManager.resetSettings();
    EEPROM.write(SETTING_INITIALIZED, 0);
    EEPROM.commit();
    delay(3000);
    ESP.restart();
  }

  // Turn on relay if auto on enabled
  doRelay(autoOn);

  // Connect to WiFi
  wifiManager.setAPCallback(wifiConfigModeCallback);
  if (!wifiManager.autoConnect(apName)) ESP.reset();

  // Turn on wifi LED to signal that we're connected
  blinkLED_tckr.detach();
  digitalWrite(LED_PIN, LOW);

  mdns.begin(mdnsName.c_str());
  server.on("/on", []() { doRelay(ON); server.send(200); });
  server.on("/off", []() { doRelay(OFF); server.send(200); });
  server.on("/status", sendStatus);
  server.on("/settings", HTTP_GET, []() { handleFileRead("/config.htm"); });
  server.on("/settings", HTTP_POST, saveSettings);
  server.on("/getsettings", getSettings);
  server.on("/time", sendTimes);  // DEBUG
  server.on("/reset", [&wifiManager]()
  {
    wifiManager.resetSettings();
    EEPROM.write(SETTING_INITIALIZED, 0);
    EEPROM.commit();
    server.send(200, "text/html", F("Clearing all saved settings (including Wifi) and restarting..."));
    delay(3000);
    ESP.restart();
  });
  server.on("/restart", []()
  {
    server.send(200, "text/html", F("<META http-equiv=\"refresh\" content=\"10;URL='/'\">Restarting..."));
    delay(3000);
    ESP.restart();
  });
  server.onNotFound([]()
  {
    if (!handleFileRead(server.uri())) server.send(404, "text/plain", "Not Found");
  });
  httpUpdater.setup(&server);
  server.begin();

  // Setup MQTT, if enabled
  if (mqttEnable)
  {
    mqtt.setServer(mqttServer.c_str(), 1883);
    mqtt.setCallback(mqttCallback);
    mqttReconnect();
  }

  // If on/off at time is enabled, start syncing the time with NTP
  if (onEnable || offEnable)
  {
    setSyncProvider(syncTime);
    setSyncInterval(NTP_SYNC_INTERVAL);
  }
}

void loop()
{
  // Handle button
  btn.read();
  if (btn.wasPressed()) (relayState ? doRelay(OFF) : doRelay(ON));

  // Handle MQTT, if enabled
  if (mqttEnable)
  {
    if (!mqtt.connected()) mqttReconnect();

    if (mqtt.connected())
    {
      if (mqttUpdate)
      {
        String topic = "outlet/" + mdnsName + "/state";
        mqtt.publish(topic.c_str(), (relayState ? "ON" : "OFF"), true);
        mqttUpdate = false;
      }
      mqtt.loop();
    }
  }

  // Handle HTTP
  server.handleClient();
}

void doRelay(bool state)
{
  digitalWrite(RELAY_PIN, state);
  relayState = state;
  if (mqttEnable) mqttUpdate = true;
}

void blinkLED()
{
  digitalWrite(LED_PIN, !digitalRead(LED_PIN));
}

void sendStatus()
{
  String response;
  IPAddress ip = WiFi.localIP();

  response = outletName;
  response += ",";
  response += relayState;
  response += ",";
  response += mdnsName;
  response += ",";
  response += mqtt.connected();
  response += ",";
  response += String(ip[0]) + "." + String(ip[1]) + "." + String(ip[2]) + "." + String(ip[3]);

  server.sendHeader("Cache-Control", "no-cache");
  server.send(200, "text/html", response);
}

time_t syncTime()
{
  if (WiFi.status() != WL_CONNECTED) return 0;

  const uint8_t NTP_PACKET_SIZE = 48;     // NTP time stamp is in the first 48 bytes of the message
  uint8_t packetBuffer[NTP_PACKET_SIZE];  // Buffer to hold incoming and outgoing packets
  IPAddress serverIP;

  // Get a random server from the NTP server pool
  WiFi.hostByName(NTP_POOL, serverIP);

  // Set all bytes in the buffer to 0
  memset(packetBuffer, 0, NTP_PACKET_SIZE);

  // Initialize values needed to form NTP request
  packetBuffer[0] = 0b11100011;   // LI, Version, Mode
  packetBuffer[1] = 0;            // Stratum, or type of clock
  packetBuffer[2] = 6;            // Polling Interval
  packetBuffer[3] = 0xEC;         // Peer Clock Precision
  // 8 bytes of zero for Root Delay & Root Dispersion
  packetBuffer[12] = 49;
  packetBuffer[13] = 0x4E;
  packetBuffer[14] = 49;
  packetBuffer[15] = 52;

  // Send packet requesting a timestamp
  WiFiUDP udp;
  udp.begin(8888);
  while (udp.parsePacket() > 0) yield();
  udp.beginPacket(serverIP, 123);
  udp.write(packetBuffer, NTP_PACKET_SIZE);
  udp.endPacket();

  // Wait for a response
  int timeout = 0;
  while (!udp.parsePacket() && (timeout < 100))
  {
    delay(10);
    timeout++;
  }

  // No response
  if (timeout >= 100) return 0;

  // Read the packet into the buffer
  udp.read(packetBuffer, NTP_PACKET_SIZE);
  uint32_t highWord = word(packetBuffer[40], packetBuffer[41]);
  uint32_t lowWord = word(packetBuffer[42], packetBuffer[43]);
  uint32_t secsSince1900 = highWord << 16 | lowWord;
  uint32_t epoch = secsSince1900 - 2208988800UL;
  udp.stop();

  if (!isTimeSet) readEEPROMTimeSettings();
  isTimeSet = true;

  return (time_t)epoch;
}

void checkTime()
{
  time_t time = myTZ.toLocal(now(), &tcr);

  if (onEnable && nextOnTime <= time)
  {
    doRelay(ON);
    nextOnTime = onTime + nextMidnight(time);
  }
  else if (offEnable && nextOffTime <= time)
  {
    doRelay(OFF);
    nextOffTime = offTime + nextMidnight(time);
  }
}

void initEEPROMSettings()
{
  EEPROM.write(SETTING_INITIALIZED, SETTINGS_REV);  // EEPROM initialized
  EEPROM.write(SETTING_AUTO_ON,                0);  // Relay on at powerup
  EEPROM.write(SETTING_ON_ENABLE,              0);  // Turn on at time enable
  EEPROM.write(SETTING_ON_HOUR,               17);  // Turn on at time hour
  EEPROM.write(SETTING_ON_MIN,                 0);  // Turn on at time minute
  EEPROM.write(SETTING_OFF_ENABLE,             0);  // Turn off at time enable
  EEPROM.write(SETTING_OFF_HOUR,              23);  // Turn off at time hour
  EEPROM.write(SETTING_OFF_MIN,                0);  // Turn off at time minute
  EEPROM.write(SETTING_TIMEZONESTD,            5);  // Timezone STD (in hours)
  EEPROM.write(SETTING_TIMEZONEDST,            4);  // Timezone DST (in hours)
  EEPROM.write(SETTING_MQTT_ENABLE,            0);  // Enable MQTT
  EEPROM.write(SETTING_DELIM,                  0);  // Dummy setting to delimit start of strings
  writeEEPROMString(SETTING_NAME, SETTING_NAME_LENGTH, outletName);
  writeEEPROMString(SETTING_MDNS, SETTING_MDNS_LENGTH, mdnsName);
  writeEEPROMString(SETTING_MQTT_SERVER, SETTING_MQTT_SERVER_LENGTH, "");
  writeEEPROMString(SETTING_MQTT_USER, SETTING_MQTT_USER_LENGTH, "");
  writeEEPROMString(SETTING_MQTT_PASSWORD, SETTING_MQTT_PASSWORD_LENGTH, "");
  EEPROM.commit();
}

void readEEPROMSettings()
{
  if (EEPROM.read(SETTING_INITIALIZED) != SETTINGS_REV) initEEPROMSettings();

  outletName = readEEPROMString(SETTING_NAME, SETTING_NAME_LENGTH);
  mdnsName = readEEPROMString(SETTING_MDNS, SETTING_MDNS_LENGTH);
  autoOn = EEPROM.read(SETTING_AUTO_ON);
  onEnable = EEPROM.read(SETTING_ON_ENABLE);
  offEnable = EEPROM.read(SETTING_OFF_ENABLE);
  mqttEnable = EEPROM.read(SETTING_MQTT_ENABLE);
  mqttServer = readEEPROMString(SETTING_MQTT_SERVER, SETTING_MQTT_SERVER_LENGTH);
  mqttUser = readEEPROMString(SETTING_MQTT_USER, SETTING_MQTT_USER_LENGTH);
  mqttPassword = readEEPROMString(SETTING_MQTT_PASSWORD, SETTING_MQTT_PASSWORD_LENGTH);
}

void readEEPROMTimeSettings()
{
  timeSTD = (TimeChangeRule){"STD", First, Sun, Nov, 2, EEPROM.read(SETTING_TIMEZONESTD) * -60};
  timeDST = (TimeChangeRule){"DST", Second, Sun, Mar, 2, EEPROM.read(SETTING_TIMEZONEDST) * -60};
  myTZ = Timezone(timeDST, timeSTD);

  time_t time = myTZ.toLocal(now(), &tcr);

  if (onEnable)
  {
    onTime = EEPROM.read(SETTING_ON_HOUR) * SECS_PER_HOUR + EEPROM.read(SETTING_ON_MIN) * SECS_PER_MIN;
    nextOnTime = (onTime + previousMidnight(time) <= time ? onTime + nextMidnight(time) :
                                                            onTime + previousMidnight(time));
  }

  if (offEnable)
  {
    offTime = EEPROM.read(SETTING_OFF_HOUR) * SECS_PER_HOUR + EEPROM.read(SETTING_OFF_MIN) * SECS_PER_MIN;
    nextOffTime = (offTime + previousMidnight(time) <= time ? offTime + nextMidnight(time) :
                                                              offTime + previousMidnight(time));
  }

  checkTime_tckr.attach(15, checkTime);
}

String readEEPROMString(uint16_t startAddress, uint16_t lengthAddress)
{
  String returnString = "";
  uint8_t length = constrain(EEPROM.read(lengthAddress), 0, MAX_STRING_LENGTH);
  for (int i = 0; i < length; i++) returnString += (char)EEPROM.read(startAddress + i);
  return returnString;
}

void writeEEPROMString(uint16_t startAddress, uint16_t lengthAddress, String str)
{
  uint8_t length = constrain(str.length(), 0, MAX_STRING_LENGTH);
  EEPROM.write(lengthAddress, length);
  for (int i = 0; i < length; i++) EEPROM.write(startAddress + i, str[i]);
  EEPROM.commit();
}

void saveSettings()
{
  String response;

  if (server.hasArg("autoOn"))
  {
    autoOn = (server.arg("autoOn").toInt() ? true : false);
    EEPROM.write(SETTING_AUTO_ON, autoOn);
  }
  if (server.hasArg("onEnable"))
  {
    onEnable = (server.arg("onEnable").toInt() ? true : false);
    EEPROM.write(SETTING_ON_ENABLE, onEnable);
  }
  if (server.hasArg("offEnable"))
  {
    offEnable = (server.arg("offEnable").toInt() ? true : false);
    EEPROM.write(SETTING_OFF_ENABLE, offEnable);
  }
  if (server.hasArg("onHour"))
  {
    uint8_t hour = server.arg("onHour").toInt();
    if (server.hasArg("onAMPM") && server.arg("onAMPM") == "1")
    {
      if (hour == 12) hour = 0;
      else hour += 12;
    }
    EEPROM.write(SETTING_ON_HOUR, constrain(hour, 0, 23));
  }
  if (server.hasArg("onMin")) EEPROM.write(SETTING_ON_MIN, constrain(server.arg("onMin").toInt(), 0, 59));
  if (server.hasArg("offHour"))
  {
    uint8_t hour = server.arg("offHour").toInt();
    if (server.hasArg("offAMPM") && server.arg("offAMPM") == "1")
    {
      if (hour == 12) hour = 0;
      else hour += 12;
    }
    EEPROM.write(SETTING_OFF_HOUR, constrain(hour, 0, 23));
  }
  if (server.hasArg("offMin")) EEPROM.write(SETTING_OFF_MIN, constrain(server.arg("offMin").toInt(), 0, 59));
  if (server.hasArg("timezoneSTD")) EEPROM.write(SETTING_TIMEZONESTD, server.arg("timezoneSTD").toInt());
  if (server.hasArg("timezoneDST")) EEPROM.write(SETTING_TIMEZONEDST, server.arg("timezoneDST").toInt());
  if (server.hasArg("name"))
  {
    outletName = server.arg("name");
    writeEEPROMString(SETTING_NAME, SETTING_NAME_LENGTH, outletName);
  }
  if (server.hasArg("mdnsName"))
  {
    mdnsName = server.arg("mdnsName");
    writeEEPROMString(SETTING_MDNS, SETTING_MDNS_LENGTH, mdnsName);
  }
  if (server.hasArg("mqttEnable"))
  {
    mqttEnable = (server.arg("mqttEnable").toInt() ? true : false);
    EEPROM.write(SETTING_MQTT_ENABLE, mqttEnable);
  }
  if (server.hasArg("mqttServer"))
  {
    mqttServer = server.arg("mqttServer");
    writeEEPROMString(SETTING_MQTT_SERVER, SETTING_MQTT_SERVER_LENGTH, mqttServer);
  }
  if (server.hasArg("mqttUser"))
  {
    mqttUser = server.arg("mqttUser");
    writeEEPROMString(SETTING_MQTT_USER, SETTING_MQTT_USER_LENGTH, mqttUser);
  }
  if (server.hasArg("mqttPassword"))
  {
    mqttPassword = server.arg("mqttPassword");
    writeEEPROMString(SETTING_MQTT_PASSWORD, SETTING_MQTT_PASSWORD_LENGTH, mqttPassword);
  }
  EEPROM.commit();

  response = F("<META http-equiv=\"refresh\" content=\"10;URL='/settings'\">Settings saved! Restarting to take effect...<br><br>");
  server.send(200, "text/html", response);
  delay(1000);
  ESP.reset();
}

void getSettings()
{
  String response;
  response = outletName;
  response += ",";
  response += mdnsName;
  response += ",";
  response += mqttServer;
  response += ",";
  response += mqttUser;
  response += ",";
  response += mqttPassword;
  response += ",";

  for (uint8_t i = 1; i < SETTING_DELIM; i++)
  {
    response += EEPROM.read(i);
    response += ",";
  }

  server.sendHeader("Cache-Control", "no-cache");
  server.send(200, "text/plain", response);
}

// TODO: Left in for debugging. Can be removed later.
void sendTimes()
{
  String response;
  response = String(myTZ.toLocal(now(), &tcr));
  response += ", ";
  response += String(onTime);
  response += ", ";
  response += String(nextOnTime);
  response += ", ";
  response += String(offTime);
  response += ", ";
  response += String(nextOffTime);
  server.send(200, "text/plain", response);
}

bool handleFileRead(String path)
{
  if (path.endsWith("/")) path += "index.htm";

  String dataType = "text/plain";
  if (path.endsWith(".htm")) dataType = "text/html";
  else if (path.endsWith(".css")) dataType = "text/css";

  if (SPIFFS.exists(path))
  {
    File file = SPIFFS.open(path, "r");
    server.streamFile(file, dataType);
    file.close();
    return true;
  }
  return false;
}

void mqttReconnect()
{
  static long lastReconnect = 0;

  if (millis() - lastReconnect > 5000)
  {
    if (mqtt.connect(mdnsName.c_str(), mqttUser.c_str(), mqttPassword.c_str()))
    {
      String topic = "outlet/" + mdnsName + "/command";
      mqtt.subscribe(topic.c_str());
      lastReconnect = 0;
    }
    else lastReconnect = millis();
  }
}

void mqttCallback(char* topic, byte* payload, unsigned int length)
{
  if (!strncasecmp_P((char*)payload, "on", length)) doRelay(ON);
  else if (!strncasecmp_P((char*)payload, "off", length)) doRelay(OFF);
}

void wifiConfigModeCallback(WiFiManager *myWiFiManager)
{
  blinkLED_tckr.attach_ms(1000, blinkLED);
}
