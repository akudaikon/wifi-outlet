var outletName = document.getElementById('name'),
    mdnsName = document.getElementById('mdnsName'),
    autoOn = document.getElementById('autoOn'),
    onEnable = document.getElementById('onEnable'),
    onDiv = document.getElementById('onDiv'),
    onHour = document.getElementById('onHour'),
    onMin = document.getElementById('onMin'),
    onAMPM = document.getElementById('onAMPM'),
    offEnable = document.getElementById('offEnable'),
    offDiv = document.getElementById('offDiv'),
    offHour = document.getElementById('offHour'),
    offMin = document.getElementById('offMin'),
    offAMPM = document.getElementById('offAMPM'),
    mqttEnable = document.getElementById('mqttEnable'),
    mqttDiv = document.getElementById('mqttDiv'),
    mqttServer = document.getElementById('mqttServer'),
    mqttUser = document.getElementById('mqttUser'),
    mqttPassword = document.getElementById('mqttPassword'),
    mqttCommandTopic = document.getElementById('mqttCommandTopic'),
    mqttStateTopic = document.getElementById('mqttStateTopic'),
    timezone = document.getElementById('timezone'),
    timezoneSTD = document.getElementById('timezoneSTD'),
    timezoneDST = document.getElementById('timezoneDST');

checkMax = function() {
  if (this.value.length > 2) this.value = this.value.slice(0, 2);
};

checkNum = function() {
  if (parseInt(this.value) > parseInt(this.max)) this.value = this.max;
  this.value = pad(this.value, 2);
};

pad = function(str, max) {
  str = str.toString();
  return str.length < max ? pad("0" + str, max) : str;
}

onHour.addEventListener("input", checkMax);
onHour.addEventListener("change", checkNum);
onMin.addEventListener("input", checkMax);
onMin.addEventListener("change", checkNum);
offHour.addEventListener("input", checkMax);
offHour.addEventListener("change", checkNum);
offMin.addEventListener("input", checkMax);
offMin.addEventListener("change", checkNum);

onEnable.onchange = function() {
  onDiv.style.display = (this.checked ? 'block' : 'none');
};

offEnable.onchange = function() {
  offDiv.style.display = (this.checked ? 'block' : 'none');
};

mqttEnable.onchange = function() {
  mqttDiv.style.display = (this.checked ? 'block' : 'none');
};

timezone.onchange = function() {
  timezoneDST.value = parseInt(timezone.value, 10) - 1;
};

$(function(){
  $('.spinner .btn:first-of-type').on('click', function() {
    var btn = $(this);
    var input = btn.closest('.spinner').find('input');
    if (input.attr('max') == undefined || parseInt(input.val()) < parseInt(input.attr('max'))) {
      input.val(pad(parseInt(input.val(), 10) + 1, 2));
    } else {
      btn.next("disabled", true);
    }
  });
  $('.spinner .btn:last-of-type').on('click', function() {
    var btn = $(this);
    var input = btn.closest('.spinner').find('input');
    if (input.attr('min') == undefined || parseInt(input.val()) > parseInt(input.attr('min'))) {
      input.val(pad(parseInt(input.val(), 10) - 1, 2));
    } else {
      btn.prev("disabled", true);
    }
  });
})

getSettings = function()
{
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4) {
      if (xhttp.status == 200) {
        var settings = xhttp.responseText.split(",");
        outletName.value = settings[0];
        mdnsName.value = settings[1];
        mqttServer.value = settings[2];
        mqttUser.value = settings[3];
        mqttPassword.value = settings[4];
        autoOn.checked = (settings[5] == "1" ? true : false);
        autoOn.value = (settings[5] == "1" ? 1 : 0);
        onEnable.checked = (settings[6] == "1" ? true : false);
        onEnable.value = (settings[6] == "1" ? 1 : 0);
        offEnable.checked = (settings[7] == "1" ? true : false);
        offEnable.value = (settings[7] == "1" ? 1 : 0);
        mqttEnable.checked = (settings[14] == "1" ? true : false);
        mqttEnable.value = (settings[14] == "1" ? 1 : 0);
        mqttCommandTopic.innerHTML = "<h6>outlet/" + settings[1] + "/command</h6>";
        mqttStateTopic.innerHTML = "<h6>outlet/" + settings[1] + "/state</h6>";

        if (settings[7] == 0) {
          onHour.value = 12;
          $('#onAMPM').val(0);
        }
        else if (settings[7] > 12) {
          onHour.value = pad(settings[7] - 12, 2);
          $('#onAMPM').val(1);
        } else {
          onHour.value = pad(settings[7], 2);
          $('#onAMPM').val(0);
        }
        onMin.value = pad(settings[8], 2);

        if (settings[10] == 0) {
          offHour.value = 12;
          $('#offAMPM').val(0);
        }
        else if (settings[10] > 12) {
          offHour.value = pad(settings[10] - 12, 2);
          $('#offAMPM').val(1);
        } else {
          offHour.value = pad(settings[10], 2);
          $('#offAMPM').val(0);
        }
        offMin.value = pad(settings[11], 2);
        $('#onAMPM').selectpicker('refresh');
        $('#offAMPM').selectpicker('refresh');

        $('#timezone').val(settings[12]);
        $('#timezoneDST').val(parseInt(settings[9], 10) - 1)
        $('#timezone').selectpicker('refresh');

        onDiv.style.display = (onEnable.checked ? 'block' : 'none');
        offDiv.style.display = (offEnable.checked ? 'block' : 'none');
        mqttDiv.style.display = (mqttEnable.checked ? 'block' : 'none');
      }
    }
  };

  xhttp.open("GET", "getsettings", true);
  xhttp.send();
}

// To get around onLoad not firing on back
setTimeout(getSettings, 100);

$('input[type="checkbox"]').change(function(){ this.value = this.checked ? 1 : 0; });