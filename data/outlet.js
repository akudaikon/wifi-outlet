var getStatus,
    button = document.getElementById('button'),
    state = document.getElementById('state'),
    outletName = document.getElementById('outletName'),
    ip = document.getElementById('ip');

var mqttOptions =
{
  keepalive: 10,
  clientId: 'Browser-' + Math.random().toString(16).substr(2, 8),
  protocolId: 'MQTT',
  protocolVersion: 4,
  clean: true,
  reconnectPeriod: 5000,
  connectTimeout: 30 * 1000,
  rejectUnauthorized: false
}

getSettings = function()
{
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4) {
      if (xhttp.status == 200) {
        var settings = xhttp.responseText.split(",");
        document.title = settings[0];
        outletName.innerHTML = "<strong>" + settings[0] + "</strong>";
        button.disabled = false;
      
        // MQTT Enabled
        if (settings[14] == "1")
        {
          mqttOptions['username'] = settings[3];
          mqttOptions['password'] = settings[4];
          var client = mqtt.connect('ws://' + settings[2] + ':1884', mqttOptions);

          client.subscribe("outlet/" + settings[1] + "/state");

          client.on("message", function (topic, payload)
          {
            console.log(topic);
            console.log(payload.toString());
            
            if (payload == "ON")
            {
              button.checked = true;
              state.innerHTML = '<i class="state-led on"></i>On';
            }
            else
            {
              button.checked = false;
              state.innerHTML = '<i class="state-led off"></i>Off';
            }
          });

          button.onchange = function()
          {
            if (button.checked == true) { client.publish("outlet/" + settings[1] + "/command", "ON"); }
            else { client.publish("outlet/" + settings[1] + "/command", "OFF"); }
          }
        }
        // MQTT Disabled - use AJAX for everything
        else
        {
          button.onchange = function()
          {
            var xhttp = new XMLHttpRequest();

            if (button.checked == true)
            {
              xhttp.open("GET", "on", true);
              xhttp.send();
              state.innerHTML = '<i class="state-led on"></i>On';
            }
            else
            {
              xhttp.open("GET", "off", true);
              xhttp.send();
              state.innerHTML = '<i class="state-led off"></i>Off';
            }
          }
          getStatusAJAX();
        }
      }
    }
  };

  xhttp.open("GET", "getsettings", true);
  xhttp.send();
}

getStatusAJAX = function()
{
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function()
  {
    if (xhttp.readyState == 4 && xhttp.status == 200)
    {
      var status = xhttp.responseText.split(",");

      if (status[1] > 0)
      {
        button.checked = true;
        state.innerHTML = '<i class="state-led on"></i>On';
      }
      else
      {
        button.checked = false;
        state.innerHTML = '<i class="state-led off"></i>Off';
      }

      ip.innerHTML = "<small>MQTT " + (status[3] > 0 ? "Connected" : "Disconnected") + "<br>" + status[2] + ".local<br>IP Address: " + status[4] + "</small>";
    }
  };
  xhttp.open("GET", "status", true);
  xhttp.send();
  setTimeout(getStatusAJAX, 5000);
};

// To get around onLoad not firing on back
setTimeout(getSettings, 100);